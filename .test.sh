# poor man's CI

set -e

run_tests()
{
	echo $'\n''### define 1'
	~/usr/git/linuxscru/linuxscru define v6.5
	echo $'\n''### define 2'
	~/usr/git/linuxscru/linuxscru define 13619170303878e
	echo $'\n''### define 3'
	~/usr/git/linuxscru/linuxscru --internal-testing define b3883ab5e95713e --stable
	echo $'\n''### define 4 (a stable commit, test 1)'
	~/usr/git/linuxscru/linuxscru --internal-testing define 70f49f7b9aa3df
	echo $'\n''### define 5 (a stable commit, test 2)'
	~/usr/git/linuxscru/linuxscru --internal-testing define aa665c3a2aca2f
	echo $'\n''### greplog'
	~/usr/git/linuxscru/linuxscru greplog "Teach lockdep that icc_bw_lock is needed in code paths that could"
	echo $'\n''### history'
	~/usr/git/linuxscru/linuxscru history Documentation/.gitignore
	echo $'\n''### maintainer'
	~/usr/git/linuxscru/linuxscru maintainer Documentation/.gitignore
	echo $'\n''### show 1'
	~/usr/git/linuxscru/linuxscru show v6.5
	echo $'\n''### show 2'
	~/usr/git/linuxscru/linuxscru show 13619170303878e
	echo $'\n''### show 3'
	~/usr/git/linuxscru/linuxscru show "interconnect: Teach lockdep about icc_bw_lock order"
	echo $'\n''### show 4'
	~/usr/git/linuxscru/linuxscru show Documentation/.gitignore
	echo $'\n''### show 5'
	~/usr/git/linuxscru/linuxscru show Documentation/.gitignore --next
	echo $'\n''### show 6'
	~/usr/git/linuxscru/linuxscru show Documentation/.gitignore --mainline
	echo $'\n''### topmerge 1'
	~/usr/git/linuxscru/linuxscru topmerge 13619170303878e
	echo $'\n''### topmerge 2'
	~/usr/git/linuxscru/linuxscru topmerge "interconnect: Teach lockdep about icc_bw_lock order"
	echo $'\n''### topmerge 3'
	~/usr/git/linuxscru/linuxscru topmerge bbb8ceb5e24211
}

cd ~/usr/git/linux.git/all/
run_tests 2>&1 | tee ~/usr/git/linuxscru/.result.new || :
echo '#################################################################'$'\n'
if diff -Naur ~/usr/git/linuxscru/.result ~/usr/git/linuxscru/.result.new; then
	echo $'\n''SUCCEEDED'
else
	echo $'\n''FAILED'
	exit 1
fi
rm ~/usr/git/linuxscru/.result.new

